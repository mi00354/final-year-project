# Final Year Project
My final year project is the implementation of an iOS application that uses computer vision models in order to classify Batik patterns. The initial idea for this project stems from my own lack of awareness of Indonesian culture. My exposure to the art of Batik have only come from the times I had to wear Batik clothing because of a dresscode and history class in school. With the rise in awareness of cultural appropriation issues world wide, I wanted to take this opportunity to educate myself on the art of Batik and its cultural significance. Through the process of researching for my project I have not only gained knowledge in computer vision techniques but also learned about a precious art from my home country. 

Possible applications of the project are for shopping online/in person, education, and automatic tagging (such as for online retail)

# Project Structure
The project can be broken down into two parts. The first part is the proof of concept. Before working on the main idea, I wanted to make sure that the technology I decided to use was feasible for the applications of the project. This involved developing a very simple application that will detect fruits using a phone camera in real time. A popular cnn architecture of moderate size was chosen at random for classifying the fruit images, as the final model for the main project won't be decided until the end. 

The second part was implementing the Batik classification mobile app. The most significant change to the app is that real time video detection was not used as it is not necessary or it can even make it more difficult for the intended applications of the app. 

# How to use the apps 
## Fruit detection
Upon first opening it will ask the user's permission for access to the phone camera (the app won't work if permission is not granted). It will then display the camera feed to the user. When the camera is pointed at a fruit it recognises, it will display its name on the bottom of the screen. If the classsifier has a low confidence no text will be displayed, so when pointed at an unrecognised fruit or background nothign will happen.

## Batik classsifier
Upon first loading the app it will also asks the users' permission for acces to the camera and in addition the camera roll. In this app users can choose to upload a photo from their camera roll or take a photo. After uploading a photo, users are then asked to crop the image to a square. After submitting, the app will display the type of Batik uploaded with its confidence, and as short description stating its origin and history/meaning.

# How to run
## ipynb files
The ipynb files contain the code used to create the classifier. This can be uploaded to colab.research.google.com and run via Runtime>Run all

## XCode Project files
These are the app files. XCode and an iOS device is required to run the project.
1. Open the project in XCode, make sure that the mlmodel file in in the project (contains the model created in the ipynb files). 
2. Connect an iOS Device to the computer and select it from the drop down menu on the top. It should be among other emulators.
3. Click run and accept the permissions upon loading the app
