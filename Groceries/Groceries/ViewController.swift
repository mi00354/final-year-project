//
//  ViewController.swift
//  Test
//
//  Created by Michelle Putri Iskandar on 23/03/2021.
//
//  This code is a modified version of the following tutorial https://www.youtube.com/watch?v=p6GA8ODlnX0

import UIKit
import AVKit // Audio visual library
import Vision

class ViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        //  Do any additional setup after loading the view.
        
        //--    Set up the camera for the app   --//
        setupCaptureSession()
        view.addSubview(label)
        setupLabel()

    }
    
    func setupCaptureSession(){
        //  Set capture session  //
        let captureSession = AVCaptureSession() // Create capture session
        captureSession.sessionPreset = .photo // Set session as photo
        
        //  Set up camera input //
        guard let captureDevice = AVCaptureDevice.default(for: .video) else {return} // Get the back camera
        guard let input = try? AVCaptureDeviceInput(device: captureDevice) else {return} // Set back camera as the an input
        captureSession.addInput(input) // Set the input for the capture session
        
        //  Start capture session  //
        captureSession.startRunning()
        
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession) // Create capture preview
        previewLayer.frame = view.frame
        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        view.layer.addSublayer(previewLayer)
        
        let dataOutput = AVCaptureVideoDataOutput() // Create a video data output object
        dataOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "videoQueue"))
        captureSession.addOutput(dataOutput) // Set the output for the capture session
    }
    
    
    //  Capture output function
    //  For testing: print to console to confirm frame has beem captured
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        //print("Camera was able to capture a frame ", Date()) // Print to console if capture is successful
        
        guard let pixelBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {return} // Get the sample buffer and cast it into a pixel buffer
        
        guard let model = try? VNCoreMLModel(for:Groceries().model) else {
            print("model not loading")
            return
            
        } // Get the model
        
        let start = CFAbsoluteTimeGetCurrent()
        // Create a request
        let request = VNCoreMLRequest(model: model) { (finishedReq, err) in
            let stop = CFAbsoluteTimeGetCurrent()
            let duration = Int32(1000 * (stop - start))
            // Check error here
            //print(finishedReq.results)
            
            guard let results = finishedReq.results as? [VNClassificationObservation] else {return} // Get the result as a VNClassificationObservation array
            
            guard let firstObservation = results.first else {return}
            
            if(firstObservation.confidence>0.8){
                DispatchQueue.main.async(execute: {
                                self.label.text = "\(firstObservation.identifier)"
                            })
            }
            else{
                DispatchQueue.main.async(execute: {
                                self.label.text = "Background"
                            })
            }
            
            
            
            print(firstObservation.identifier, firstObservation.confidence)
            print(String(format: "Duration : %5d ms", duration))
        }
        
        try? VNImageRequestHandler(cvPixelBuffer: pixelBuffer, options: [:]).perform([request])
        
    }
    
    let label: UILabel = {
            let label = UILabel()
            label.textColor = .white
            label.translatesAutoresizingMaskIntoConstraints = false
            label.text = "Label"
            label.font = label.font.withSize(30)
            return label
        }()

    func setupLabel() {
            label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            label.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50).isActive = true
    }


}

