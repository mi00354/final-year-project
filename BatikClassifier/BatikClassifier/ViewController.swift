//
//  ViewController.swift
//  BatikClassifier
//
//  Created by Michelle Putri Iskandar on 16/05/2021.
//

import UIKit
import CoreML
import Vision

class ViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {

    @IBOutlet weak var motif: UILabel!
    @IBOutlet weak var confLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var photoButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }


    @IBAction func cameraButton(_ sender: Any) {
        
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            
             let alert = UIAlertController(title: "No camera", message: "This device does not support camera.", preferredStyle: .alert)
             let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
             alert.addAction(ok)
             self.present(alert, animated: true, completion: nil)
             return
            
           }
           
           let picker = UIImagePickerController()
           
           picker.sourceType = .camera
           picker.delegate = self
           picker.allowsEditing = true
           picker.cameraCaptureMode = .photo
        
           present(picker, animated: true, completion: nil)
    }
    
    @IBAction func photoButton(_ sender: Any) {
        
        guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else {
            let alert = UIAlertController(title: "No photos", message: "This device does not support photos.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
            return
            
        }
     
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Dismiss the picker controller
        dismiss(animated: true)
        
        // guard unwrap the image picked
        guard let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            fatalError("couldn't load image")
        }
        
        // Set the picked image to the UIImageView - imageView
        imageView.image = image
        
        // Convert UIImage to CIImage to pass to the image request handler
        guard let ciImage = CIImage(image: image) else {
            fatalError("couldn't convert UIImage to CIImage")
        }
         
        detectMotif(image: ciImage)
     }
    
    func detectMotif(image: CIImage) {
        motif.text = "Detecting motif…"
     
        // Load the ML model through its generated class
        guard let model = try? VNCoreMLModel(for: BatikClassifier().model) else {
              fatalError("can't load Batik model")
        }
     
        // Create request for Vision Core ML model loaded
        let request = VNCoreMLRequest(model: model) { [weak self] request, error in
               guard let results = request.results as? [VNClassificationObservation],
                    let topResult = results.first else {
                    fatalError("unexpected result type from VNCoreMLRequest")
               }
            let motifName = "\(topResult.identifier)"
            let conf = ((topResult.confidence)*1000).rounded() / 10
            
            print(topResult.identifier, topResult.confidence)
            // Update the UI on main queue
            DispatchQueue.main.async { [weak self] in
                self?.motif.text = "Batik " + motifName.capitalized
                self?.confLabel.text = "Confidence: \(conf)%"
                self?.descLabel.text = self?.getDesc(desc: motifName)
            }
        }
     
        
     
        // Run the Core ML AgeNet classifier on global dispatch queue
        let handler = VNImageRequestHandler(ciImage: image)
        DispatchQueue.global(qos: .userInteractive).async {
          do {
              try handler.perform([request])
          } catch {
              print(error)
          }
        }
    }
    
    func getDesc(desc: String) -> String{
        
        switch desc{
        case "kawung":
            return "Batik kawung is a geometric motif made of ellipse based shapes mimicking the shape of the kawung fruit. This motif is also often interpreted as a lotus flower with four blooming petals.The kawung batik represents the hope that people will always remember their roots. This motif comes from Yogyakarta."
        
        case "megamendung":
            return "Batik megamendung is a non geometric motif made of shapes based on clouds. The clouds are usually triangular in shape. The motif is an Indonesian adaptation to the cloud motif introduced by Chinese merchants. This motif comes from Cirebon."
        
        case "parang":
            return "Batik parang is a geometric motif made of continuous diagonal lines. The lines are seperated by a connected S like pattern. This continuous motif represents the waves of the ocean symbolizing the will to never give up. This motif comes from Yogyakarta."
    
        case "sidomukti":
            return "Batik sidomukti is a nongeometric motif consisting of symmetric angles with different ornaments within them. There is usually a main ornament, examples of the ornaments are butterflies, birds or flowers. The meaning of the motif depends on the main ornaments in them. This motif comes from Surakarta."
            
        case "truntum":
            return "Batik truntum is a geometric motif made up of stars (these stars can often look like flowers). This motif represents a person's sincere love due to its history. This motif comes from Surakarta."
        
        default:
            return ""
}
    }
}




